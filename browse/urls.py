from django.urls import include, path
from django.conf.urls import url
from django.views.generic import RedirectView

from . import views
from .filters import RecordFilter

urlpatterns = [
    path('', views.browse, name='browse'),
    url('filter', views.filter, name='filter'),
    url('develop', views.develop, name='develop'),
    url('export', views.export, name='export'),
    url('download_file', views.download_file, name='download_file'),
    url('save_options', views.save_options, name='save_options'),
    url('generate_all_pdf', views.generate_all_pdf, name='generate_all_pdf'),
    url('import_reports', views.import_reports, name='import_reports'),
    url('update_without_import', views.update_without_import, name='update_without_import'),
]
