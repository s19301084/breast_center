from django.contrib import admin
from django.utils.html import format_html

from import_export.admin import ImportExportModelAdmin
from .models import Record, Guideline

@admin.register(Record)
class RecordAdmin(ImportExportModelAdmin):
        list_display = (
        'id',
        'birth',
        'pathology_type',
        'surgery_type',
        'pathology_t',
        'pt_size',
        'pathology_n',
        'pn_size',
        'clinical_t',
        'ct_size',
        'clinical_n',
        'clinical_m',
        'clinical_stage',
        'aln_num',
        'aln_pos',
        'pathology_stage',
        'grade_br',
        'grade_nuclear',
        'er_status',
        'er',
        'pr_status',
        'pr',
        'her2',
        'fish',
        'her2_status',
        'mib1',
        )


@admin.register(Guideline)
class GuidelineAdmin(ImportExportModelAdmin):
        list_display = (
                'id', 
                'priority',                 
                'type', 
                'description', 
                'prescription_list', 
                'comment',                 
                )
        # ordering = ('type', 'description', 'priority')
        ordering = ('id', 'priority', 'type', 'description')

        def prescription_list(self, obj):
                return format_html("<br />NAME: ".join(obj.prescription.split("NAME: ")).strip("<br />"))
        
        prescription_list.short_description = 'Prescription (Please follow the "NAME: ... FREQUENCY: ... NAME: ..." format)'
        prescription_list.allow_tags = True