from datetime import date
import csv
import glob
import os
import re
import time
import pandas as pd
import json

from .config import DEBUG, empty_string
from django.db import models
from django.conf import settings
from django_mysql.models import ListTextField
from django.http import HttpResponse


class Record(models.Model): # change fields to reach factors
    id = models.CharField(max_length=20, default=empty_string, primary_key=True, verbose_name='病歷號', help_text='不需加"-"，e.g. 1234567-8，為12345678')
    birth = models.CharField(max_length=20, default='0600101', null=True, verbose_name='生日', help_text='年月日，e.g. 民國88年5月6號，為0880506')
    pathology_type = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='病理型態', help_text='e.g. IDC / ILC / DCIS / mucinous(invasive) ...')
    clinical_t = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='臨床T (cT)', help_text='e.g. 1a / 4b / 2')
    ct_size = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='cTsize(cm)',  help_text='數字(可含小數點)，不含單位公分 e.g. 2.5 / 0.3*0.5')
    clinical_n = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='臨床N (cN)')
    clinical_m = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='臨床M (cM)', help_text='e.g. 0 / 1')
    clinical_stage = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='臨床stage (c stage)', help_text='e.g. 1a / 1b / 2a / 2b ...')
    pathology = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='病理分期 (pTNM)', help_text='e.g. pT2N1 / ypT0N0')
    surgery_type = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='手術方式', help_text='e.g. partial / simple / MRM')
    pathology_t = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='病理T (pT)', help_text='e.g. 1a / 1b / 1c / 2 ... / 1mi')
    pt_size = models.CharField(max_length=20, default='x', null=True, verbose_name='pTsize(cm)', help_text='不含單位，e.g. 2.3 / 0.5*0.7*1.2')
    pathology_n = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='病理N (pN)', help_text='e.g. 1 / 2 / 2a / 3 / 3b / 1mi ...')
    pn_size = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='pNsize(cm)', help_text='不含單位，e.g. 2.3')
    aln_num = models.IntegerField(default=0, null=True, verbose_name='ALN num', help_text='Total Dissected LN Number')
    aln_pos = models.IntegerField(default=0, null=True, verbose_name='ALN pos', help_text='Positive LN Number')
    pathology_stage = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='病理stage')
    grade_br = models.CharField(max_length=20, default=-99, null=True, verbose_name='GRADE(BR)')
    grade_nuclear = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='GRADE(Nuclear)')
    er_status = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='ER status', help_text='(1=positive, 0=negative)')
    er = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='ER(%)', help_text='數入數值，不用加%')
    pr_status = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='PR status', help_text='(1=positive, 0=negative)')
    pr = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='PR(%)', help_text='數入數值，不用加%')
    her2_status = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='HER2 status', help_text='(1=positive, 0=negative, 2=equivocal, 9=未檢驗/無數值)')
    her2 = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='HER2(IHC)', help_text='0 / 1+ / 2+ / 3+')
    fish = models.CharField(max_length=20, default=9, null=True, verbose_name='FISH', help_text='(1=positive, 0=negative, 2=equivocal, 9=未檢驗/無數值)')
    mib1 = models.CharField(max_length=20, default=empty_string, null=True, verbose_name='MIB1(%)/Ki67(%)', help_text='數入數值，不用加%')
    histologic_type = models.CharField(max_length=50, null=True, default=empty_string)
    lymph_size = models.FloatField(null=True, blank=True, default=empty_string)
    max_pt_size = models.FloatField(null=True, blank=True, default=empty_string)
    neoadjuvant = models.BooleanField(default=empty_string, blank=True, null=True, verbose_name='術前藥物治療', help_text='如果需要輸出Neoadjuvant則改為Yes')
    comment = models.TextField(default=empty_string, blank=True, null=True, verbose_name='補充建議')

    # rule based
    cancer_subgroup = models.TextField(default=empty_string, blank=True, null=True, verbose_name='乳癌子類別')
    radiotherapy = models.CharField(max_length=20, blank=True, null=True, default=empty_string, verbose_name='放療')
    treatment_list = ListTextField(
        base_field=models.CharField(max_length=255, blank=True, null=True, default=empty_string),
        size=100,  # Maximum of 100 ids in list
        default=empty_string,
        verbose_name='治療方式'
    )
    report_html = models.TextField(default=empty_string, blank=True, null=True, verbose_name='報告html')
    original_report_text = models.TextField(default=empty_string, blank=True, null=True, verbose_name='原始報告txt')
    
    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in Record._meta.fields]

    def save(self, *args, **kwargs):
        # decide the subgroup of cancer before saving
        list_of_files = glob.glob('./guideline/*') # * means all if need specific format then *.csv
        latest_file = max(list_of_files, key=os.path.getctime)
        full_guideline = pd.read_csv(f"{latest_file}", sep='\t')

        self.cancer_subgroup = self.generate_cancer_subgroup()
        self.radiotherapy = self.generate_radiotherapy()
        self.treatment_list = self.generate_treatment_list()
        self.report_html = self.generate_report_html(full_guideline)
        super(Record, self).save(*args, **kwargs)

    def update(self, *args, **kwargs):
        # decide the subgroup of cancer before saving
        list_of_files = glob.glob('./guideline/*') # * means all if need specific format then *.csv
        latest_file = max(list_of_files, key=os.path.getctime)
        full_guideline = pd.read_csv(f"{latest_file}", sep='\t')

        self.cancer_subgroup = self.generate_cancer_subgroup()
        self.radiotherapy = self.generate_radiotherapy()
        self.treatment_list = self.generate_treatment_list()
        self.report_html = self.generate_report_html(full_guideline)
        super(Record, self).update(*args, **kwargs)


    def generate_cancer_subgroup(self):
        """ determine the subgroup of cancer """

        if self.er == empty_string:
            er = -1
        elif str(self.er)[0] == '<': 
            er = -1
        else:
            er = int(str(self.er).split("(")[0])
        if self.pr == empty_string:
            pr = -1
        elif str(self.pr)[0] == '<': 
            pr = -1
        else:
            pr = int(str(self.pr).split("(")[0])    
        if self.her2_status == empty_string:
            her2_status = -1
        elif str(self.her2_status)[0] == '<': 
            her2_status = -1
        else:
            her2_status = int(str(self.her2_status).split("(")[0])    
        if self.er_status == empty_string:
            er_status = -1
        elif str(self.er_status)[0] == '<': 
            er_status = -1
        else:
            er_status = int(str(self.er_status).split("(")[0])   
        if self.pr_status == empty_string:
            pr_status = -1
        elif str(self.pr_status)[0] == '<': 
            pr_status = -1
        else:
            pr_status = int(str(self.pr_status).split("(")[0])  
        if self.fish == empty_string:
            fish = -1
        elif str(self.fish)[0] == '<': 
            fish = -1
        else:
            fish = int(str(self.fish).split("(")[0]) 
        if self.mib1 == empty_string:
            mib1 = -1
        elif str(self.mib1)[0] == '<': 
            mib1 = -1
        else:
            mib1 = int(str(self.mib1).split("(")[0]) 

        if str(self.grade_br)[0] == '<':
            grade_br = empty_string
        elif str(self.grade_br)[0] == 'x':
            grade_br = -99
        else:
            grade_br = int(str(self.grade_br).split("(")[0])

        if er > 50 and pr >= 20 and (her2_status == 0 or fish == 0) and (mib1 < 20 or grade_br == 1 or grade_br == 2):
            return "Luminal A-like"
        elif (er_status == 1 and (her2_status == 0 or fish == 0)) and (pr < 20 or mib1 >= 20 or grade_br == 3 or er < 50):
            return "Luminal B-like HER negative"
        elif er_status == 1 and (her2_status == 1 or fish == 1):
            return "Luminal B-like HER positive"
        elif er_status == 0 and pr_status == 0 and (her2_status == 1 or fish == 1):
            return "HER2 positive"
        elif er < 10 and pr < 10 and (her2_status == 0 or fish == 0):
            return "Triple negative"
        else:
            print(f"{self.id}: cancer_subgroup_unknown")
            return "Unknown"

    def generate_radiotherapy(self):
        """ determine whether radiotherapy is need """
        pt_size_list = str(self.pt_size).split("*")
        max_pt_size = 0
        for size in pt_size_list:
            if size.replace('.','',1).isdigit():
                if max_pt_size < float(size):
                    max_pt_size = float(size)
            else:
                max_pt_size = -99

        if self.surgery_type:
            if "partial" in self.surgery_type:
                return "Yes"

            elif "lumpectomy" in self.surgery_type:
                return "Yes"

            elif "MRM"  in self.surgery_type or "simple" in self.surgery_type:
                # N1: Node 1-3(+) \ N2: Node 4-9(+) \ N3: Node >10
                if (self.pathology_t[0].isdigit() and int(self.pathology_t[0]) < 3) and \
                (self.pathology_n[0].isdigit() and int(self.pathology_n[0]) <= 1):
                    return "No"
                elif (((self.pathology_t[0].isdigit() and int(self.pathology_t[0]) >= 3) or max_pt_size > 5) or \
                    ((self.pathology_n[0].isdigit() and int(self.pathology_n[0]) > 2) or int(self.aln_pos) >= 4)):
                    return "Yes"
            else:
                return "Unknown"
        else:
            return "Unknown"

    def generate_treatment_list(self):
        if self.er == empty_string:
            er = -1
        elif str(self.er)[0] == '<': 
            er = -1
        else:
            er = int(str(self.er).split("(")[0])
        if self.pr == empty_string:
            pr = -1
        elif str(self.pr)[0] == '<': 
            pr = -1
        else:
            pr = int(str(self.pr).split("(")[0])    
        if self.her2_status == empty_string:
            her2_status = -1
        elif str(self.her2_status)[0] == '<': 
            her2_status = -1
        else:
            her2_status = int(str(self.her2_status).split("(")[0])    
        if self.er_status == empty_string:
            er_status = -1
        elif str(self.er_status)[0] == '<': 
            er_status = -1
        else:
            er_status = int(str(self.er_status).split("(")[0])   
        if self.pr_status == empty_string:
            pr_status = -1
        elif str(self.pr_status)[0] == '<': 
            pr_status = -1
        else:
            pr_status = int(str(self.pr_status).split("(")[0])  
        if self.fish == empty_string:
            fish = -1
        elif str(self.fish)[0] == '<': 
            fish = -1
        else:
            fish = int(str(self.fish).split("(")[0]) 
        if self.mib1 == empty_string:
            mib1 = -1
        elif str(self.mib1)[0] == '<': 
            mib1 = -1
        else:
            mib1 = int(str(self.mib1).split("(")[0]) 

        if len(str(self.birth)) == 8:  # 19511012
            age = date.today().year - int(str(self.birth)[:4])
        else: # 0761012
            age = date.today().year - 1911 - int(str(self.birth)[:3])

        tmp_treatment = []
        # 術前藥物治療
        if self.neoadjuvant:
            if "Luminal A-like" in self.cancer_subgroup:
                tmp_treatment.append("Luminal A-like | NeoAdjuvant")
            elif "Luminal B-like HER positive" in self.cancer_subgroup:
                tmp_treatment.append("Luminal B-like HER positive | NeoAdjuvant")
            elif "Luminal B-like HER negative" in self.cancer_subgroup:
                tmp_treatment.append("Luminal B-like HER negative | NeoAdjuvant")
            elif "HER2 positive" in self.cancer_subgroup:
                tmp_treatment.append("HER2 positive | NeoAdjuvant")
            elif "Triple negative" in self.cancer_subgroup:
                tmp_treatment.append("Triple negative | NeoAdjuvant")
        else:
            # 化療
            if er_status == 1 and (her2_status == 0 or fish == 0):
                """
                (luminal-A like): 
                (pT1mi | pT1a | pT1b) & pN0 → hormone therapy alone
                pT1cN0: Suggest MammaPrint to omit cheomtherapy
                pT2N0: 
                1. Suggest MammaPrint to omit cheomtherapy (SDM)
                1-options:  E(90)C(600) Q3W x 4-6  
                1-options:  T(75)C(600) Q3W x 4-6 (T self-pay).
                1-options:  E(90)C(600) Q3W x 4 then T(75) Q3W x 4 (T self-pay)

                pT3N0:
                1. T(75)C(600) Q3W x 4-6 (T self-pay)
                1-options: E(90)C(600) Q3W x 4-6 
                1-options: E(90)C(600) Q3W x 4 then T(75) Q3W x 4 (T self-pay)
                1-options: Suggest MammaPrint to omit cheomtherapy if postmenopause (SDM)

                pN1(1): 
                1. E(90)C(600) x 4 + T(75) x 4.
                1-options: T(75)C x6
                1-options: no Chemo if MammaPrint low risk. 
                
                pN1(2-3) | pT4:  
                1. E(90)C(600) x 4 + T(75) x 4 ,
                1-options: T(75)C x6
                1-options: CMF x6 
                """
                if "Luminal A-like" in self.cancer_subgroup:
                    if (self.pathology_t[0:3] == '1mi' or 
                    self.pathology_t[0:2] == '1a' or 
                    self.pathology_t[0:2] == '1b') and self.pathology_n[0] == '0':
                        tmp_treatment.append("(ER = 1) and (HER2 = 0 or FISH = 0) and (pT1mi or pT1a or pT1b) and (pN0) and (Luminal A-like) | Chemotherapy")
                    elif self.pathology_t[0:2] == '1c' and self.pathology_n[0] == '0':
                        tmp_treatment.append("(ER = 1) and (HER2 = 0 or FISH = 0) and (pT1c) and (pN0) and (Luminal A-like) | Chemotherapy")
                    elif self.pathology_t[0] == '2' and self.pathology_n[0] == '0':
                        tmp_treatment.append("(ER = 1) and (HER2 = 0 or FISH = 0) and (pT2*) and (pN0) and (Luminal A-like) | Chemotherapy")
                    elif self.pathology_t[0] == '3' and self.pathology_n[0] == '0':
                        tmp_treatment.append("(ER = 1) and (HER2 = 0 or FISH = 0) and (pT3) and (pN0) and (Luminal A-like) | Chemotherapy")
                    elif self.pathology_n[0] == '1' and self.aln_pos == '1' : # actual node num =1
                        tmp_treatment.append("(pN1*) and (ALN_POS = 1) | Chemotherapy")
                    elif (self.pathology_n[0] == '1' and 
                    (self.aln_pos == '2' or 
                    self.aln_pos == '3')) or (self.pathology_t[0] == '4'): # pN1(2-3) | pT4
                        tmp_treatment.append("((pN1*) and ((ALN_POS = 2) or (ALN_POS = 3)) or pT4) | Chemotherapy")

                elif "Luminal B-like" in self.cancer_subgroup:
                    if self.pathology_n[0] == '0' : 
                        tmp_treatment.append("(ER = 1) and (HER2 = 0 or FISH = 0) and (pN0) and (Luminal B-like) | Chemotherapy")
                    elif self.pathology_n[0] == '1': 
                        tmp_treatment.append("(ER = 1) and (HER2 = 0 or FISH = 0) and (pN1) and (Luminal B-like) | Chemotherapy")
                elif self.pathology_n[0] == '2' or self.pathology_n[0] == '3':
                    if "Luminal A-like" in self.cancer_subgroup or "Luminal B-like" in self.cancer_subgroup:
                        tmp_treatment.append("(ER = 1) and (HER2 = 0 or FISH = 0) and (pN2 or pN3) and (Luminal A-like or Luminal B-like) | Chemotherapy")
                else:
                        tmp_treatment.append("Unknown | Unknown")

            if (her2_status == 1 or fish == 1):
                if self.pathology_n[0] == '0':
                    tmp_treatment.append("(HER2 = 1 or FISH = 1) and (pN0) | Chemotherapy")
                    tmp_treatment.append("(HER2 = 1 or FISH = 1) and (pN0) | Target")
                elif self.pathology_n[0] == '1' or self.pathology_n[0] == '2' or self.pathology_n[0] == '3': 
                    tmp_treatment.append("(HER2 = 1 or FISH = 1) and (pN1 or pN2 or pN3) | Chemotherapy")
                    tmp_treatment.append("(HER2 = 1 or FISH = 1) and (pN1 or pN2 or pN3) | Target") # 標靶藥
                else:
                    tmp_treatment.append("Unknown | Unknown")
                    tmp_treatment.append("Unknown | Unknown") # 標靶藥

            if self.cancer_subgroup == 'Triple negative':
                tmp_treatment.append("(HER2 = 1 or FISH = 1) and (Triple negative) | Chemotherapy")
                if self.pathology_n[0] == '2' or self.pathology_n[0] == '3': 
                    tmp_treatment.append("(HER2 = 1 or FISH = 1) and (Triple negative) and (pN2 or pN3) | Chemotherapy - dose dense")
                if self.pathology_n[0] == '1' or self.pathology_n[0] == '2' or self.pathology_n[0] == '3': 
                    tmp_treatment.append("(HER2 = 1 or FISH = 1) and (Triple negative) and (pN1 or pN2 or pN3) | Chemotherapy - maintain")

            # 放療
            if self.surgery_type:
                if 'partial' in self.surgery_type or \
                ('lumpectomy' in self.surgery_type) or \
                ('MRM' in self.surgery_type and \
                (self.pathology_t[0] != '1' and self.pathology_t != '0') and \
                (self.pathology_n[0] != '1' and self.pathology_n[0] != '0')):
                    tmp_treatment.append("(Partial or Lumpectomy or MRM) and (pT >= 2) and (pN >= 2) | Radiotherapy")


            # 賀爾蒙
            if er_status == 1 and (her2_status== 0 or fish == 0):
                if self.pathology_n[0] == '0' and (self.pathology_t == '1mic' or self.pathology_t == '1a' or self.pathology_t == '1b'):
                    if age < 50:
                        tmp_treatment.append("(pN0 and (pT1mic or pT1a or pT1b)) and (age < 50) | Endocrine")
                    else:
                        tmp_treatment.append("(pN0 and (pT1mic or pT1a or pT1b)) and (age >= 50) | Endocrine")
                elif ((self.pathology_t == '1c' or self.pathology_t == '2') and self.pathology_n[0] == '0') or (self.pathology_n[0] == '1'):
                    if age < 50:
                        tmp_treatment.append("(((pT1c or pT2) and pN0) or (pN1*)) and (age < 50) | Endocrine")
                    else:
                        tmp_treatment.append("(((pT1c or pT2) and pN0) or (pN1*)) and (age >= 50) | Endocrine")
                elif (self.pathology_t[0] == '3' or self.pathology_t[0] == '4') or (self.pathology_n[0] == '2' or self.pathology_n[0] == '3'):
                    if age < 50:
                        tmp_treatment.append("((pT3* or pT4*) or (pN2* or pN3*)) and (age < 50) | Endocrine")
                    else:
                        tmp_treatment.append("((pT3* or pT4*) or (pN2* or pN3*)) and (age >= 50) | Endocrine")
                else:
                    tmp_treatment.append("Unknown | Unknown")

        if "Unknown" in tmp_treatment or tmp_treatment == []:
            print(f"{self.id}: tmp_treatment_unknown")

        return tmp_treatment

    def generate_report_html(self, full_guideline):

        if len(str(self.birth)) == 8:  # 19511012
            age = date.today().year - int(str(self.birth)[:4])
        else: # 0761012
            age = date.today().year - 1911 - int(str(self.birth)[:3])
        
        # post-process the attribute
        if str(self.er_status)[0] == '1':
            er_status_s = '+'
        else:
            er_status_s = '-'

        if str(self.pr_status)[0] == '1':
            pr_status_s = '+'
        else:
            pr_status_s = '-'

        if str(self.her2_status)[0] == '1':
            her2_status_s = 'positive'
        elif (str(self.her2_status)[0] == '0' and str(self.her2) == '2'):
            her2_status_s = 'equivocal'
        else:
            her2_status_s = 'negative'

        max_pt_size = str(self.pt_size) 
        multiply_symbol = ["*", "x", "X"]
        
        if str(self.pt_size) is not "x" and any(x in str(self.pt_size) for x in multiply_symbol):
            max_pt_size = max([float(x) for x in str(self.pt_size).split(re.sub('[\d|.]','', str(self.pt_size))[0])])


        surgery_type = str(self.surgery_type).replace("simple", "Simple Mastectomy")

        treatment_table = """
        <tr>
            <th colspan=1>選項</th>
            <th>順序</th>
            <th>治療類別</th>
            <th colspan=3>處方</th>
            <th colspan=1>療程頻率</th>
        </tr>  

        """

        order = 0

        for treatment_code in self.treatment_list:
            treatment_code = treatment_code.strip() # Luminal A-like | NeoAdjuvant
            filter_guideline = full_guideline[(full_guideline['description'] == treatment_code.split(" | ")[0]) &
            (full_guideline['type'] == treatment_code.split(" | ")[1])]

            for index, row in filter_guideline.iterrows():
                guideline_id = row['id'] # 01-01
                guideline_treatment = row['description'] + ' | ' + row['type'] # Luminal A-like NeoAdjuvant
                if treatment_code == guideline_treatment: # need to be printed
                    if not isinstance(row['comment'], (int, float)):
                        comment = row['comment'] # only need one comment

                    counter = 0
                    
                    if row['priority'] == 'option':
                        priority = 'Another option of '  
                    else:
                        priority =  ''
                        order += 1

                    prescription_list = ["NAME"+e for e in row['prescription'].split("NAME") if e] 
                    p_len = len(prescription_list)
                    name_pattern = re.compile(r'NAME: (.*)FREQUENCY')
                    frequency_pattern = re.compile(r'FREQUENCY: (.*)$')

                    if p_len == 1:
                        name = name_pattern.findall(prescription_list[0])[0].strip()
                        frequency = frequency_pattern.findall(prescription_list[0])[0].strip()                        
                        treatment_table += f"""
                                        <tr id="{str(guideline_id)}_{str(guideline_treatment)}">
                                            <th rowspan={str(p_len)} colspan=1> <input type="checkbox" checked="checked" onchange="input_click(this)"> </th>
                                            <th rowspan={str(p_len)}>{str(priority) + str(order)}</th>
                                            <th rowspan={str(p_len)}>{str(row['type'])}</th>
                                            <th colspan=3><strong style="color: #005eba;">{str(name)}</strong></th>
                                            <th><strong style="color: #005eba;">{str(frequency)}</strong></th>
                                        </tr>
                                        """

                    else:
                        treatment_table += f"""
                            <tr id="{str(guideline_id)}_{str(guideline_treatment)}">
                                <th rowspan={str(p_len+1)} colspan=1> <input type="checkbox" checked="checked" onchange="input_click(this)"> </th>
                                <th rowspan={str(p_len+1)}>{str(priority) + str(order)}</th>
                                <th rowspan={str(p_len+1)}>{str(row['type'])}</th>
                            </tr>
                            """
                        for p_num in range(0, len(prescription_list)):
                            counter += 1
                            name = name_pattern.findall(prescription_list[p_num])[0].strip()
                            frequency = frequency_pattern.findall(prescription_list[p_num])[0].strip()
                            treatment_table += f"""
                                            <tr id="{str(guideline_id)}_{str(guideline_treatment)}_{str(counter)}">
                                            <th colspan=3><strong style="color: #005eba;">{str(name)}</strong></th>
                                            <th><strong style="color: #005eba;">{str(frequency)}</strong></th>
                                            </tr> 
                                            """


        css_s = '<strong style="color: #005eba;">'
        css_e = '</strong>'

        if len(str(self.pathology)) > 0 and str(self.pathology)[0] == 'y' or self.neoadjuvant:
            pathology_prefix = 'y'
        else:
            pathology_prefix = ''

        report_html = f"""
                  <h2>癌症治療計畫書 Cancer Treatment Plan</h2>
                  <br>

                    <h4>台北榮民總醫院 (CTP/2019/001-201912051356 N01. 00)</h4>

                        <table>
                        <tr>
                            <th>姓名</th>
                            <th>身分證字號</th>
                            <th>性別</th>
                            <th>出生日期</th>
                            <th>年齡</th>
                            <th>病歷號碼</th>
                            <th>多專科團隊</th>
                            <th>計畫書起草日</th>
                        </tr>
                        <tr>
                            <td>測試姓名</td>
                            <td>HXXXXX1234</td>
                            <td>F</td>

                    """
        if len(str(self.birth)) == 8:  # 19511012
            report_html += f"""
             <td id="birth_s">{int(str(self.birth)[:4])-1911}年{str(self.birth)[4:6]}月{str(self.birth)[6:]}日</td>
            """
        else: # 0761012
            report_html += f"""
             <td id="birth_s">{str(self.birth)[:3]}年{str(self.birth)[3:5]}月{str(self.birth)[5:]}日</td>
            """
        object_fields = {x[0]: x[1] for x in self.get_fields()} 
        report_html +=  f"""
                        <td id="age">{age}</td>
                        <td id="id">{self.id}</td>
                        <td>乳癌</td>
                        <td id="create_time"></td>

                    </tr>

                    </table>
                    <br>

                <h4>原始癌症期別: </h4>
                    <tr>

                """

        adjuvant_str = f"""Neoadjuvant""" if self.neoadjuvant else f"""Adjuvant"""

        if self.clinical_t is not empty_string:      
            report_html += f"""
                                <p>Clinical TNM Stage: cT{css_s}{self.clinical_t}{css_e}N{css_s}{self.clinical_n}{css_e}M{css_s}{self.clinical_m}{css_e}, (c_stage: {css_s}{str(self.clinical_stage).upper()}{css_e})
                            """
        report_html += f"""
                            <p>Pathology TNM Stage: {pathology_prefix}pT{css_s}{self.pathology_t}{css_e}N{css_s}{self.pathology_n}{css_e} ({css_s}{int(self.aln_pos)}{css_e}/{css_s}{int(self.aln_num)}{css_e}),  pTsize: {css_s}{max_pt_size}{css_e}cm, (p_stage: {css_s}{str(self.pathology_stage).upper()}{css_e})

                    <h4>原始重要預後因子: </h4>
                    <div class="row_two">
                        <div class="column_two">
                            <p>ER:   <strong style="color: #005eba;">{self.er} %, {er_status_s}</strong></p>
                            <p>PR:   <strong style="color: #005eba;">{self.pr} %, {pr_status_s}</strong></p>
                            <p>HER2-IHC:   <strong style="color: #005eba;">{self.her2}, {her2_status_s}</strong></p>
                            <p>MIB-1/Ki67:   <strong style="color: #005eba;">{self.mib1} %</strong></p>
                        </div>
                        <div class="column_two">
                            <p>Grade:   <strong style="color: #005eba;">{self.grade_nuclear}</strong></p>
                            <p>Intrinstic Type:   <strong style="color: #005eba;">{self.cancer_subgroup}</strong></p>
                            <p>Surgery Type:   <strong style="color: #005eba;">{surgery_type}</strong></p>
                            <p>Pathologic Diagnosis:   <strong style="color: #005eba;">{self.histologic_type}</strong></p>
                        </div>
                    </div>                



                    <br>
                    <h4>治療計畫內容:</h4>
                    <p>治療計畫分類: 新診斷首次療程(即時治療)</p>
                    <p>療程別: 002</p>
                    <p>治療目的: CURATIVE</p>
                    <p>治療指引: VGH Guideline</p>
                    <p>治療順序驗碼: 4 ({adjuvant_str} systemic Therapy (Chemotherpy/target therapy/radiotherpy/hormone therapy)</p>

                    <div>
                        <table id="treatment_table">
                         {treatment_table}
                        </table>
                    </div>
                    <div>
                    {self.comment if self.comment else ''}
                    </div>
                    <br>
                    """
        if DEBUG:
            for key in object_fields:
                if key != 'report_html':
                    report_html += f"<div>{key}: {object_fields[key]}</div>" 
        report_html += f"""
                    <br>
                    <p>後續治療: 有，指定給下一會診科別或醫生: DOCXXXXX XXXXX 醫師</p>
                    <p>補充說明: F/U REPONE</p>

                    <br>
                    <h4>治療計畫依據:</h4>
                    <p></p>
                    <p>主治醫師: DOCXXXXX XXX 醫師 &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;個案管理師: NURXXXXX XXX 管理師</p>

                    <br>

        """

        return report_html


class Guideline(models.Model):
    id = models.CharField(max_length=20, primary_key=True)
    description = models.TextField(default='Empty Description', blank=True, null=True,)
    type = models.CharField(max_length=50, default='Empty Type')
    prescription = models.TextField(default='Empty Prescription', blank=True, null=True)
    priority = models.CharField(max_length=20, default='Empty Priority')
    comment = models.TextField(default='', blank=True, null=True,)

    def save(self, *args, **kwargs):
        opts = Guideline.objects.all().model._meta
        field_names = [field.name for field in opts.fields]
        print (os.getcwd())
        with open(f'./guideline/Guideline-{time.strftime("%Y%m%d-%H%M%S")}.csv', 'w') as csvfile:
            writer = csv.writer(csvfile, delimiter='\t')
            # Write a first row with header information
            writer.writerow(field_names)
            # Write data rows
            for obj in Guideline.objects.all():
                writer.writerow([getattr(obj, field) for field in field_names])
        super(Guideline, self).save(*args, **kwargs)
