import django_filters
from django import forms

from .models import Record


class RecordFilter(django_filters.FilterSet):

    def create_choice(filter_target):
        return [(i, i) for i in Record.objects.values_list(filter_target, flat=True).distinct()]

    case_id = django_filters.CharFilter(lookup_expr='icontains')
    birth = django_filters.CharFilter(lookup_expr='icontains')
    clinical_t = django_filters.ChoiceFilter(choices=create_choice('clinical_t'))
    clinical_n = django_filters.ChoiceFilter(choices=create_choice('clinical_n'))
    clinical_m = django_filters.ChoiceFilter(choices=create_choice('clinical_m'))
    clinical_stage = django_filters.ChoiceFilter(choices=create_choice('clinical_stage'))
    surgery_type = django_filters.ChoiceFilter(choices=create_choice('surgery_type'))
    pathology_t = django_filters.ChoiceFilter(choices=create_choice('pathology_t'))
    pathology_n = django_filters.ChoiceFilter(choices=create_choice('pathology_n'))
    pathology_type = django_filters.ChoiceFilter(choices=create_choice('pathology_type'))
    pathology_stage = django_filters.ChoiceFilter(choices=create_choice('pathology_stage'))

    aln_num = django_filters.RangeFilter()
    aln_pos = django_filters.RangeFilter()

    class Meta:
        model = Record
        fields = ['case_id', 'birth', 'clinical_t', 'clinical_n', 'clinical_m', 'clinical_stage', 'surgery_type', 'pathology_stage', 'aln_num', 'aln_pos', 'her2']