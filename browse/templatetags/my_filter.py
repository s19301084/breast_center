from django import template
from django.template.defaultfilters import stringfilter
register = template.Library()

@register.filter(is_safe=True)
def add_xx(value):
    if len(value) == 8:  # 19511012
        return f'{int(value[:4])-1911}/{value[4:6]}/{value[6:]}' 
    else: # 0761012
        return f'{value[:3]}/{value[3:5]}/{value[5:7]}'