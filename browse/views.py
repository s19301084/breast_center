import os
import re
import time

import pdfkit
import pandas as pd

from django.shortcuts import render
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.conf import settings
from django.urls import reverse
from django.db.models import signals


from .models import Record
from .filters import RecordFilter
from .task import generate_pdf
from .pattern_extractor import pattern_extract

options = {
        'encoding': 'UTF-8'
    }

css = [os.path.join(settings.STATICFILES_DIRS[0], 'css', 'table.css'),
       os.path.join(settings.STATICFILES_DIRS[0], 'css', 'base.css'),]


def browse(request):
    records = Record.objects.all()
    return render(request, 'browse.html', {'records': records})


def develop(request):
    return render(request, 'develop.html')


def create_record(sender, instance, created, *args, **kwargs):
    input_html = """
        <div id="myModal" class="modal">
            <div class="modal-content">
        """ + instance.report_html + """
            </div>
        </div>
        """
    generate_pdf(instance.id, input_html)


def generate_all_pdf(request):
    signals.post_save.connect(create_record, sender=Record)
    print(len(Record.objects.all()))
    for idx, obj in enumerate(Record.objects.all()):
        print(f"update: {idx}")
        obj.save()
    return render(request, 'browse.html', {'records': Record.objects.all()})


def update_without_import(request):
    for obj in Record.objects.all():
        obj.save()
    return render(request, 'browse.html', {'records': Record.objects.all()})


def import_reports(request):
    df = pd.read_csv(f'./original_data/report.csv')
    breast_df =  df[(df['ITEM'] == 'PATH') & (df['OUTCOME'].str.contains("breast"))]
    grouped_dfs = breast_df.groupby("CHARTID")
    birth_df = pd.read_csv("./original_data/100_bitrh_20210113.csv")
    birth_dict = dict(zip(birth_df.病歷號, birth_df.生日))

    for group, grouded_df in grouped_dfs:
        # print(f'{group}: {len(grouded_df)}')
        
        # select which report to use
        surgery_type_matches = ["lumpectomy", "mastectomy", "excision", "axillary dissection", "setinel biopsy"]
        malignant_matches = ['carcinoma', 'metastasis', 'metastatic']
        count = 0
        for index, row in grouded_df.iterrows():
            # exclude needle biopsy, and some should contain one of the terms in surgery_type_matches
            if ('needle biopsy' not in (row['OUTCOME'].lower())) \
            and any(x in row['OUTCOME'].lower() for x in surgery_type_matches):

                # only need the malignant report
                if any(x in row['OUTCOME'].lower() for x in malignant_matches):
                    dic = pattern_extract(row['OUTCOME'], group, row.PFKEY, birth_dict)                        

                    dic['original_report_text'] = row['OUTCOME']
                    exist_key = [str(x).replace("browse.Record.", "") 
                                        for x in Record._meta.fields
                                        if str(x).replace("browse.Record.", "") in dic 
                                        and dic[str(x).replace("browse.Record.", "")] != '']

                    r = Record(**{k: v for k, v in dic.items() if k in exist_key})
                    r.save() 

    return render(request, 'browse.html', {'Record': Record.objects.all()})



def filter(request):
    records = Record.objects.all()
    records_filter = RecordFilter(request.POST, queryset=records)
    return render(request, 'filter.html', {'filter': records_filter})


def export(request):
    body = request.POST.get('innerHTML')
    body = body.replace('<span class="close">×</span>', "") 
    global case_id 
    case_id = re.search('<td id="id">(.*)</td>', body).group(1)
    Record.objects.filter(id=case_id).update(report_html=body)
    filename = 'report/' + case_id + '.pdf'
    if os.path.isfile(filename):
        print(filename)
        os.remove(filename)

    body_pdf = generate_pdf_format(body)
    pdfkit.from_string(body_pdf, filename, options=options, css=css)
    return HttpResponse("")  # returns the response.


def download_file(request):
    global case_id 
    filename = 'report/' + case_id + '.pdf'
    pdf = None 
    while pdf is None:
        try:
            pdf = open(filename, 'rb')
        except Exception as e:
            print(e)
            time.sleep(4)
            
    response = HttpResponse(pdf.read(), content_type='application/pdf')  # Generates the response as pdf response.
    response['Content-Disposition'] = 'attachment; filename=' + case_id + '.pdf'
    pdf.close()
    return response  # returns the response.



def save_options(request):
    body = request.POST.get('innerHTML')
    body = body.replace('<span class="close">×</span>', "") 
    case_id = re.search('<td id="id">(.*)</td>', body).group(1)
    Record.objects.filter(id=case_id).update(report_html=body)
    filename = 'report/' + case_id + '.pdf'
    if os.path.isfile(filename):
        os.remove(filename) # ensure not to save the old one

    return HttpResponse("")  # returns the response.



def generate_pdf_format(body):
    body =  """
    <div id="myModal" class="modal">

        <div class="modal-content">""" + body + """
        </div>
    </div> 
    """ # to have css style
    matchObj = re.findall(r'<tr(.*?)</tr>', body, re.M|re.I|re.S)
    for idx, text in enumerate(matchObj):
        if 'input type="checkbox"' in text and not 'checked="checked"' in text:
            rowspan_num = int(text[text.find("rowspan")+len("rowspan=")+1])
            for i in range(rowspan_num):
                body = body.replace(matchObj[idx+i].strip(),"")

    # Eliminate checkbox shown in PDF
    body = body.replace('<th colspan="1">選項</th>', "") 
    body = body.replace('<th rowspan="4" colspan="1"> <input type="checkbox" checked="checked" onchange="input_click(this)"> </th>', "") 
    body = body.replace('<th rowspan="3" colspan="1"> <input type="checkbox" checked="checked" onchange="input_click(this)"> </th>', "") 
    body = body.replace('<th rowspan="2" colspan="1"> <input type="checkbox" checked="checked" onchange="input_click(this)"> </th>', "") 
    body = body.replace('<th rowspan="1" colspan="1"> <input type="checkbox" checked="checked" onchange="input_click(this)"> </th>', "")
    body = body.replace('<th rowspan="4" colspan="1"> <input type="checkbox" onchange="input_click(this)" checked="checked"> </th>', "")
    body = body.replace('<th rowspan="3" colspan="1"> <input type="checkbox" onchange="input_click(this)" checked="checked"> </th>', "")
    body = body.replace('<th rowspan="2" colspan="1"> <input type="checkbox" onchange="input_click(this)" checked="checked"> </th>', "")
    body = body.replace('<th rowspan="1" colspan="1"> <input type="checkbox" onchange="input_click(this)" checked="checked"> </th>', "")

    body = body.replace('Another option of ', "")

    return body


