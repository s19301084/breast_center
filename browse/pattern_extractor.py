import re
import json
from .config import DEBUG, empty_string

def is_fraction_in_brackets(s):
    return re.findall('\((\d+)\/(\d+)',s)

def is_number_in_brackets(s):
    return re.findall('\((\d+)\)',s)

def raw_pattern_extract(text, pid, report_id, birth_dict):
    # data structure of the record pattern
    dic = {# 'name': '',
           'id': pid, # pid? # filename?
           'report_id': report_id, 
           'birth': birth_dict[pid],
           'pathology_type': '', # done
           'tissues_size': '', # done
           'max_pt_size': '', # done
           # 'clinical_t': '', # not find
           # 'ct_size': '', # not find
           # 'clinical_n': '', # not find
           # 'clinical_m': '', # not find
           # 'clinical_stage': '', # not find
           'pathology': '', # post preprocess
           'surgery_type': '', # done     
           'pathology_t': '', # need confirm 
           'pt_size': '', # done   
           'pathology_n': '', # ??????????????
           'pn_size': '', # post preprocess
           'aln_num': 0, # post preprocess
           'aln_pos': 0, # post preprocess
           'pathology_stage': '', # 
           'grade_br': '', # done  
           'grade_nuclear': '', # done
           'er_status': '', # done              
           'er': '', # done                      
           'pr_status': '', # done                
           'pr': '', # done                       
           'her2_status': '', # done                 
           'her2': '', # done                         
           'mib1': '', # done                            
           'fish': '', # done  
           'ki67': '', # done  
           'her2_amp': '', # done # extra col
           'l_invasion': '', # done # extra col
           'margins': '', # done # extra col
           'tumor_necrosis': '', # done # extra col
           'tumor_infiltrating': '', # done # extra col
           'gross_find': '', # done # extra col
           'nodal_status': '', # done # extra col
           'nodal_status_aln': '', # done # extra col
           'positive_nodes': '', # done # extra col
           'macrometastases': 0, # done # extra col
           'micrometastases': 0, # done # extra col
           'isolated_tumor_cells': 0, # done # extra col
           'tubule': '', # done # extra col
           'pleomorphism': '', # done # extra col
           'mitoses': '', # done # extra col
           'microcalcifications': '', # done # extra col
           'in_situ_component': '', # done # extra col
           'lymph_size': 0,
           'need_check': 0 ,
           'neoadjuvant': False,
           'histologic_type': '',
           }                           


    text_before_comment = text.split("Comment:")[0]    

    pathology_diagnosis_pattern = re.compile(r'PATHOLOGICAL DIAGNOSIS:\s.*(?:\n[ ]*\S[^GROSS FINDING].*)*', re.MULTILINE)
    pathology_diagnosis = pathology_diagnosis_pattern.findall(text)[0]
    if pathology_diagnosis:
        dic['pathology_diagnosis'] = pathology_diagnosis
        pathology_diagnosis_list = dic['pathology_diagnosis'].split('\n')[1:]
        dic['surgery_type'] = "\n".join([x for x in pathology_diagnosis_list if x[0].isnumeric()])
        dic['pathology_type'] = "\n".join([x for x in pathology_diagnosis_list if x[0] == "-"])

    else:
        dic['pathology_diagnosis'] = empty_string
        
       
    gross_find = re.search(r'(GROSS FINDING)[\S\n ]+(MICROSCOPIC FINDING)', text)
    if gross_find:
        dic['gross_find'] = gross_find.group(0)

        tissue_pattern = re.compile(r'(\d+(?:\.\d+)?\s*[X|x]*\s*){3}(cm|mm)', re.MULTILINE)
        tissues = re.finditer(r'(\d+(?:\.\d+)?\s*[X|x]*\s*){3}(cm|mm)', dic['gross_find'])
        dic['tissues_size'] = [x.group().replace("\n", "") for x in tissues]    
    
    # skip txt files not needed
#     if ("breast" not in str(pathDiagnosist).lower()) and ("lymph node" not in str(pathDiagnosist).lower())\
#     and ("soft tissue, left axillary" not in str(pathDiagnosist).lower()) and ("soft tissue, right axillary" not in str(pathDiagnosist).lower()):
#         return dic

    carcinoma_size_pattern = re.compile(r'Size of invasive carcinoma( \((cm|mm)\))?:\s*(\d+(\.\d+)?)(x\d+(\.\d+)?)?\s*((\()?(cm|mm)(\))?)?', re.MULTILINE)
    carcinoma_size = carcinoma_size_pattern.search(text)

    carcinoma_size_pattern_v2 = re.compile(r'Size of invasive carcinoma( \((cm|mm)\))?:', re.MULTILINE)
    carcinoma_size_v2 = carcinoma_size_pattern_v2.search(text)


    if carcinoma_size:
        # if DEBUG:
        #     print(pid)
        #     print(carcinoma_size.group(0))
        #     print(carcinoma_size.group(1))
        #     print(carcinoma_size.group(2))
        #     print(carcinoma_size.group(3))
        #     print(carcinoma_size.group(4))
        #     print(carcinoma_size.group(5))
        #     print(carcinoma_size.group(6))
        if 'cm' in carcinoma_size.group(0):
            dic['pt_size'] = carcinoma_size.group(3)
            if carcinoma_size.group(5) != None:
                dic['pt_size'] = f"{dic['pt_size']}{carcinoma_size.group(5)}"

        elif 'mm' in carcinoma_size.group(0):
            dic['pt_size'] = carcinoma_size.group(3)

        else:
            dic['pt_size'] = f"{carcinoma_size.group(3)}: no unit"
    
    elif "in situ" in text:
        dic['pt_size'] = "0.0"
    elif carcinoma_size_v2:
        dic['pt_size'] = "0.0"
    else:
        dic['pt_size'] = empty_string
        
    dic['max_pt_size'] = dic['pt_size'] 
    multiply_symbol = ["*", "x", "X"]
    
    if dic['pt_size']:
        if dic['pt_size'] is not "x" and any(x in dic['pt_size'] for x in multiply_symbol):
            dic['max_pt_size'] = max([float(x) for x in dic['pt_size'].split(re.sub('[\d|.]','', dic['pt_size'])[0])])

        if not any(x in dic['pt_size'] for x in multiply_symbol) and \
        '0.0' not in dic['pt_size']:
            if 'cm' in carcinoma_size.group(0):
                dic['pt_size'] = round(float(dic['pt_size']), 2)
            if 'mm' in carcinoma_size.group(0):
                dic['pt_size'] = round(float(dic['pt_size']) * 0.1, 2)
                dic['max_pt_size'] = round(float(dic['max_pt_size']) * 0.1, 2)

    # calculate aln_num and aln_pos
    nodal_status = re.findall(r'Nodal status: [\S\n ]+Positive nodes', text)
    positive_nodes = re.search(r'Positive nodes: ((.*\n){2})', text)

    if nodal_status:
        dic['nodal_status'] = nodal_status[0]
    else:
        dic['nodal_status'] = empty_string
            
    if positive_nodes:
        if "isolated tumor cell" not in positive_nodes.group(1):
            positive_nodes = re.search(r'Positive nodes: ((.*\n){1})', text)
        dic['positive_nodes'] = positive_nodes.group(1)
    else:
        dic['positive_nodes'] = empty_string
        
    if pathology_diagnosis and nodal_status and positive_nodes:
        targeted_text = dic['pathology_diagnosis'] \
                                    + dic['nodal_status']\
                                    + dic['positive_nodes']
        
        fraction_list = is_fraction_in_brackets(targeted_text)
        number_in_bucket_list = is_number_in_brackets(targeted_text)

        dic['nodal_status_aln'] = fraction_list[-1]
        del fraction_list[-1]

        macrometastases = re.search(r'macrometastases\s\(\d+\)', targeted_text)
        if macrometastases:
            dic['macrometastases'] = int(re.findall("\d+", macrometastases.group(0))[0])
            del number_in_bucket_list[-1]
            
        micrometastases = re.search(r'micrometastases\s\(\d+\)', targeted_text)
        if micrometastases:
            dic['micrometastases'] = int(re.findall("\d+", micrometastases.group(0))[0])
            del number_in_bucket_list[-1]            
       
        isolated_tumor_cells = re.search(r'isolated tumor cells\s\(\d+\)', targeted_text)
        if isolated_tumor_cells:
            dic['isolated_tumor_cells'] = int(re.findall("\d+", isolated_tumor_cells.group(0))[0])
            del number_in_bucket_list[-1]        

        # aln_num == lymph node num
        dic['aln_pos'] = sum([int(x[0]) for x in fraction_list])
        dic['aln_num'] = sum([int(x[1]) for x in fraction_list]) + sum([int(x) for x in number_in_bucket_list])

        if dic['aln_pos'] != int(dic['nodal_status_aln'][0]) or dic['aln_num'] != int(dic['nodal_status_aln'][1]):
            dic['need_check'] = 1
            
        lymph_size = re.search(r'size: (\d?.?\d?)( cm| mm)', targeted_text)
        if lymph_size:
            dic['lymph_size'] = float(lymph_size.group(1))
            if 'cm' in targeted_text :
                dic['lymph_size'] = dic['lymph_size'] * 10
        
            
    hGrade_pattern = re.compile(r'Histologic grade\s*(\()?Nottingham histologic score(\)?)?:\s*Grade\s*(\S+.*).*', re.IGNORECASE)
    hGradet = hGrade_pattern.findall(text)
    if hGradet:
        hGradet_c = str(hGradet).count("I")
        if hGradet_c < 4:
                dic['grade_br'] = hGradet_c
        else:
            dic['grade_br'] = "x"
    else:
        dic['grade_br'] = "x"

    tubule = re.search(r'Tubule (.*?); ', text)
    if tubule:
        dic['tubule'] = int(tubule.group(1))
    else:
        dic['tubule'] = empty_string
        
    pleomorphism = re.search(r'Pleomorphism\s*(.*?)(;|:) ', text, re.IGNORECASE)
    if pleomorphism:
        if pleomorphism.group(1) is not '*':
            dic['pleomorphism'] = int(pleomorphism.group(1))
            dic['grade_nuclear'] = int(pleomorphism.group(1))
        else:
            dic['pleomorphism'] = pleomorphism.group(1)
            dic['grade_nuclear'] = pleomorphism.group(1)
    else:
        dic['pleomorphism'] = empty_string
        dic['grade_nuclear'] = empty_string
        
    mitoses = re.search(r'Mitoses (.*?)\)', text)
    if tubule:
        dic['mitoses'] = int(mitoses.group(1))
    else:
        dic['mitoses'] = empty_string       
    
    otherISH_pattern = re.compile(r'Other ISH results with Her2 IHC \(2\+\) are (\S+.*).*', re.MULTILINE)
    otherISHt = otherISH_pattern.findall(text)
    if "positive" in str(otherISHt).lower():
        dic['fish'] = 1
    elif "negative" in str(otherISHt).lower():
        dic['fish'] = 0
    else:
        dic['fish'] = empty_string
        

    Ki67_pattern = re.compile(r'Ki-67 \(MIB-1\): (\S+.*).*', re.MULTILINE)
    Ki67_exist = Ki67_pattern.findall(text)
    Ki67_number_pattern = re.compile(r'(\d+)\s*%')
    
    if Ki67_exist:
        Ki67_number_exist = Ki67_number_pattern.findall(str(Ki67_exist))
        if Ki67_number_exist:
            dic['mib1'] = max([int(x) for x in Ki67_number_exist])
        else:
            dic['mib1'] = -1
    else:
        dic['mib1'] = -1
    dic['ki67'] = dic['mib1']


    ER_pattern = re.compile(r'ER(\s*\(Novocastra, clone 6F11; polymer\))?:\s*(\S+.*).*', re.MULTILINE)
    ER_exist = ER_pattern.findall(text_before_comment)
    
    ER_pattern_v2 = re.compile(r'Estrogen receptor \(ER\):\s*(\S+.*).*', re.MULTILINE)
    if not ER_exist:
        ER_exist = ER_pattern_v2.findall(text_before_comment)
 
    if ER_exist:
        if "positive" in str(ER_exist).lower() or "strong" in str(ER_exist).lower():
            dic['er_status'] = 1
        elif "negative" in str(ER_exist).lower() or "weak" in str(ER_exist).lower():
            dic['er_status'] = 0
            dic['er'] = 0

        ER_number_exist = re.compile(r'\d+\s*%').findall(str(ER_exist))
        if ER_number_exist:
            dic['er'] = int(ER_number_exist[0][:-1])
            if dic['er'] > 0:
                dic['er_status'] = 1
    else:
        dic['er_status'] = empty_string
        dic['er'] = empty_string
        

    PR_pattern = re.compile(r'PR( \(Novocastra, clone 16; polymer\))?: (\S+.*).*', re.MULTILINE)
    PR_exist = PR_pattern.findall(text_before_comment)
    
    PR_pattern_v2 = re.compile(r'Progesterone receptor \(PR\):\s*(\S+.*).*', re.MULTILINE)
    if not PR_exist:
        PR_exist = PR_pattern_v2.findall(text_before_comment)
    
    if PR_exist:
        if "positive" in str(PR_exist).lower() or "strong" in str(PR_exist).lower():
            dic['pr_status'] = 1
        elif "negative" in str(PR_exist).lower() or "weak" in str(PR_exist).lower():
            dic['pr_status'] = 0
            dic['pr'] = 0
            
        PR_number_exist = re.compile(r'\d+\s*%').findall(str(PR_exist))
        if PR_number_exist:
            dic['pr'] = int(PR_number_exist[0][:-1])
            if dic['pr'] > 0:
                dic['pr_status'] = 1
    else:
        dic['pr_status'] = empty_string
        dic['pr'] = empty_string
              

    HER2_pattern = re.compile(r'HER(-)?2(\/)?([a-zA-Z(,;)]|\s)*:\s*(Positive|Negative|Equivocal)?\s*(\((\+)?\d(\+)?\))?', re.IGNORECASE)
    HER2t = HER2_pattern.findall(text_before_comment)
    if HER2t:
        if "positive" in str(HER2t).lower() or "strong" in str(HER2t).lower():
            dic['her2_status'] = 1
        elif "negative" in str(HER2t).lower() or \
             "equivocal" in str(HER2t).lower() or \
             "weak" in str(HER2t).lower():
            dic['her2_status'] = 0
        else:
            dic['her2_status'] = empty_string

        # 0，1+都是negative
        # 2+要看FISH
        # 3+就一定是positive
        if "3+" in str(HER2t).lower() or "+3" in str(HER2t).lower():
            dic['her2'] = 3
        elif "0" in str(HER2t).lower():
            dic['her2'] = 0
        elif  "1+" in str(HER2t).lower() or "+1" in str(HER2t).lower():
            dic['her2'] = 1
        elif (("2+" in str(HER2t).lower() or "+2" in str(HER2t).lower()) and dic['fish'] == 1) \
             or (("2+" in str(HER2t).lower() or "+2" in str(HER2t).lower()) and "ductal carcinoma in situ" in dic['pathology_type'].lower()):
            dic['her2'] = 2
            dic['her2_status'] = 1
        else:
            dic['her2'] = empty_string
            
    else:
        dic['her2_status'] = empty_string
        dic['her2'] = empty_string

    margins = re.search(r'Microscopic margins: (\S+.*).*', text)
    if margins:
        dic['margins'] = margins.group(1)
    else:
        dic['margins'] = empty_string
    
    tumor_necrosis = re.search(r'Tumor necrosis: (\S+.*).*', text)
    if tumor_necrosis:
        dic['tumor_necrosis'] = tumor_necrosis.group(1)
    else:
        dic['tumor_necrosis'] = empty_string
    
    tumor_infiltrating = re.search(r'Tumor infiltrating lymphocytes: (\S+.*).*', text)
    if tumor_infiltrating:
        dic['tumor_infiltrating'] = tumor_infiltrating.group(1)
    else:
        dic['tumor_infiltrating'] = empty_string
        
    her2_amp = re.search(r'Her2/neu gene amplification: (\S+.*).*', text)
    if her2_amp:
        dic['her2_amp'] = her2_amp.group(1)
    else:
        dic['her2_amp'] = empty_string

    histologic_type = re.search(r'Histologic type: (\S+.*).*', text)
    if histologic_type:
        dic['histologic_type'] = histologic_type.group(1)
    else:
        dic['histologic_type'] = empty_string

    if dic['histologic_type']:
        dic['histologic_type'] += f' ({"".join([x[0].upper() for x in dic["histologic_type"].split(" ")])})'

    l_invasion = re.search(r'Lymphovascular invasion: (\S+.*).*', text)
    if l_invasion:
        dic['l_invasion'] = l_invasion.group(1)
    else:
        dic['l_invasion'] = empty_string
    
    microcalcifications = re.search(r'Microcalcifications: (\S+.*).*', text)
    if microcalcifications:
        dic['microcalcifications'] = microcalcifications.group(1)
    else:
        dic['microcalcifications'] = empty_string
    
    in_situ_component = re.search(r'In situ component: (\S+.*).*', text)
    if in_situ_component:
        dic['in_situ_component'] = in_situ_component.group(1)
    else:
        dic['in_situ_component'] = empty_string 

    if 'status post neoadjuvant' in text.lower():
        dic['neoadjuvant'] = True

    return dic


def post_preprocess_pattern_extract(text, dic):
    # https://docs.google.com/presentation/d/1G2OfQF0uMaSTIOfz42kDWH2GyTzHOGEb/edit#slide=id.p1
    lower_text = text.lower()
    # pT
    # Reference: https://www.ncbi.nlm.nih.gov/books/NBK65744/
    if dic['max_pt_size'] != empty_string:
        greatest_dim_mm = float(dic['max_pt_size']) * 10
        # is: DCIS (DCIS = ductal carcinoma in situ)
        if "ductal carcinoma in situ" in dic['pathology_type'].lower():
            dic['pathology_t'] = 'is'
        # 1mi: Tumor ≤1 mm in greatest dimension.
        elif greatest_dim_mm <= 1:
            dic['pathology_t'] = '1mi'
        # 1a: Tumor >1 mm but ≤5 mm in greatest dimension (round any measurement >1.0–1.9 mm to 2 mm).
        elif greatest_dim_mm > 1 and greatest_dim_mm <= 5:
            dic['pathology_t'] = '1a'
        # 1b: Tumor >5 mm but ≤10 mm in greatest dimension.
        elif greatest_dim_mm > 5 and greatest_dim_mm <= 10:
            dic['pathology_t'] = '1b'
        # 1c: Tumor >10 mm but ≤20 mm in greatest dimension.
        elif greatest_dim_mm > 10 and greatest_dim_mm <= 20:
            dic['pathology_t'] = '1c'
        # 2: Tumor >20 mm but ≤50 mm in greatest dimension.
        elif greatest_dim_mm > 20 and greatest_dim_mm <= 50:
            dic['pathology_t'] = '2'
        # 3: Tumor >50 mm in greatest dimension.
        elif greatest_dim_mm > 50:
            dic['pathology_t'] = '3'
        # 4: Tumor of any size with direct extension to the chest wall and/or to the skin (ulceration or macroscopic nodules); invasion of the dermis alone does not qualify as T4.
        elif 'chest' in dic['gross_find']: # need confirm
            dic['pathology_t'] = '4'
        # 4a: Extension to the chest wall; invasion or adherence to pectoralis muscle in the absence of invasion of chest wall structures does not qualify as T4.
        # 4b: Ulceration and/or ipsilateral macroscopic satellite nodules and/or edema (including peau d'orange) of the skin that does not meet the criteria for inflammatory carcinoma.
        # 4c: Both T4a and T4b are present.
        # 4d: Inflammatory carcinoma (see Rules for Classificationc).
    else:
        dic['pathology_t'] = '0'
        
    # pN
    # referecnce: https://www.ncbi.nlm.nih.gov/books/NBK65744/table/CDR0000062787__1943/
    if 'lymph node' not in lower_text:
        dic['pathology_n'] = 'X'
    else:
        dic['pathology_n'] = '0'
        # ITCs only (malignant cell clusters ≤0.2 mm) in regional lymph node(s).
        if (dic['isolated_tumor_cells'] > 0 and 
        dic['micrometastases'] == 0 and 
        dic['macrometastases'] == 0 and 
        dic['lymph_size'] > 0.2):
            dic['pathology_n'] = '0(i+)'
        # 1mi: Micrometastases (~200 cells, >0.2 mm, but ≤2.0 mm).
        elif (dic['micrometastases'] > 0 and 
        dic['lymph_size'] <= 2):
            dic['pathology_n'] = '1mi'
        # 1a: Metastases in 1–3 axillary lymph nodes, at least one metastasis >2.0 mm.
        elif (dic['micrometastases'] > 0 and 
        dic['aln_pos'] in range(1,4) and 
        dic['lymph_size'] > 2):
            dic['pathology_n'] = '1a'
        # 1b: Metastases in ipsilateral internal mammary sentinel nodes, excluding ITCs.
        # 1c: pN1a and pN1b combined.
        # 2a: Metastases in 4–9 axillary lymph nodes (at least 1 tumor deposit >2.0 mm).
        elif (dic['aln_pos'] in range(4,10) and 
        dic['lymph_size'] > 2):
            dic['pathology_n'] = '2a'
        # 2b: Metastases in clinically detected internal mammary lymph nodes with or without microscopic confirmation; with pathologically negative axillary nodes.
        elif (dic['aln_pos'] > 10):
            dic['pathology_n'] = '3'     
        # 3a: Metastases in ≥10 axillary lymph nodes (at least 1 tumor deposit >2.0 mm); or metastases to the infraclavicular (Level III axillary lymph) nodes.
        # 3b: pN1a or pN2a in the presence of cN2b (positive internal mammary nodes by imaging);
        # 3c: Metastases in ipsilateral supraclavicular lymph nodes.

    dic['pathology'] = f"pT{dic['pathology_t']}N{dic['pathology_n']}"
    simplified_pathology = f"pT{dic['pathology_t'][0]}N{dic['pathology_n'][0]}"
    # if DEBUG and dic['id'] == 18459284:
    #    print(json.dumps(dic, indent = 4, sort_keys=True))

    if simplified_pathology == 'pTisN0':
        dic['pathology_stage'] = '0'  # (Tis, N0, M0)
    if simplified_pathology == 'pT1N0':
        dic['pathology_stage'] = '1A'  # (T1, N0, M0)
    if simplified_pathology == 'pT0N1mi' or simplified_pathology == 'pT1N1mi':
        dic['pathology_stage'] = '1B' # (T0, N1mi, M0) or (T1, N1mi, M0)
    if simplified_pathology == 'pT0N1' or simplified_pathology == 'pT1N1' or simplified_pathology == 'pT2N0':
        dic['pathology_stage'] = '2A'  # (T0, N1, M0) or (T1, N1, M0) or (T2, N0, M0)
    if simplified_pathology == 'pT2N1' or simplified_pathology == 'pT3N0':
        dic['pathology_stage'] = '2B'  # (T2, N1, M0) or (T3, N0, M0)
    if (simplified_pathology == 'pT0N2' or 
    simplified_pathology == 'pT1N2' or 
    simplified_pathology == 'pT2N2' or 
    simplified_pathology == 'pT3N1' or 
    simplified_pathology == 'pT3N2'):
        dic['pathology_stage'] = '3A'  # (T0, N2, M0) or (T1, N2, M0) or (T2, N2, M0) or (T3, N1, M0) or (T3, N2, M0)     
    if simplified_pathology == 'pT4N0' \
    or simplified_pathology == 'pT4N1' \
    or simplified_pathology == 'pT4N2':
        dic['pathology_stage'] = '3B'  # (T4, N0, M0) or (T4, N1, M0) or (T4, N2, M0)
    if dic['pathology_n'] == '3': 
        dic['pathology_stage'] = '3C'  # Any T (Tis, T1, T0, T2, T3, T4; N3, M0)    
    # if dic['pathology_m'] == '1': 
    #    dic['pathology_stage'] = '4'
    #  Any T (Tis, T1, T0, T2, T3, T4; Any N = N0, N1mi, N1, N2, N3, M1)
    return dic


def pattern_extract(text, pid, report_id, birth_dict):
    dic = raw_pattern_extract(text, pid, report_id, birth_dict)
    dic = post_preprocess_pattern_extract(text, dic)
    return dic
