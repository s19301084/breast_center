from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import Record


class RecordResource(resources.ModelResource):
    class Meta:
        model = Record

       