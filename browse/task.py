import os
from celery import task
from django.core.mail import send_mail
from django.conf import settings
import pdfkit
import platform

options = {
        'encoding': 'UTF-8'
    }


# @task
# def send_email(recipient_email, recipient_name): 
#     subject = 'Test sending email'
#     message = 'hello {}'.format(recipient_name)
#     mail_sent = send_mail(
#             subject,
#             message,
#             settings.EMAIL_HOST_USER, # 寄件人的信箱
#             [recipient_email] # 收件人的信箱
#     )
#     return mail_sent

@task
def generate_pdf(case_id, content): 
    windows = platform.system() == 'Windows'
    if windows:
        config = pdfkit.configuration(wkhtmltopdf=r'./wkhtmltox/bin/wkhtmltopdf.exe')
    css = [os.path.join(settings.STATICFILES_DIRS[0], 'css', 'table.css'),
           os.path.join(settings.STATICFILES_DIRS[0], 'css', 'base.css'),]
    content = content.replace('<span class="close">×</span>', "") 
    filename = 'report/' + case_id + '.pdf'
    if windows:
        pdfkit.from_string(content, filename, options=options, css=css, configuration=config)
    else:
        pdfkit.from_string(content, filename, options=options, css=css)
