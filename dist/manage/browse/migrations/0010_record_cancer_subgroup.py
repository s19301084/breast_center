# Generated by Django 2.2.2 on 2020-01-08 13:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('browse', '0009_remove_record_record_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='record',
            name='cancer_subgroup',
            field=models.CharField(default='unknown', max_length=20),
        ),
    ]
